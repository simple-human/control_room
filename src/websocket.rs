use actix::prelude::*;
use actix_web::*;
use actix_web_actors::ws;
use serde_json::Value;

use crate::auth::AuthActor;
use crate::driver::Sources;
use crate::observer_manager::{Controls, Kill, ObserverManager, Subscribe, Unsubscribe};
use std::sync::Arc;

use crate::observables::Observables;

use log::{error, warn};
use serde_derive::*;
use serde_json::json;

pub struct Ws {
    observer_manager: Option<Addr<ObserverManager>>,
    auth_manager: Arc<Addr<AuthActor>>,
    sources: Arc<Sources>,
}

impl Ws {
    pub fn new(auth_manager: Arc<Addr<AuthActor>>, sources: Arc<Sources>) -> Self {
        Ws {
            observer_manager: None,
            auth_manager,
            sources,
        }
    }

    pub fn auth(&mut self, policy: Observables, ws: Addr<Ws>) {
        self.observer_manager =
            Some(ObserverManager::new(policy, Arc::clone(&self.sources), ws).start());
    }
}

impl Actor for Ws {
    #[cfg(not(test))]
    type Context = ws::WebsocketContext<Self>;
    #[cfg(test)]
    type Context = Context<Self>;

    fn stopped(&mut self, _: &mut Self::Context) {
        if let Some(a) = self.observer_manager.as_ref() {
            a.do_send(Kill)
        };
    }
}

pub struct MsgOut(pub Value);
impl Message for MsgOut {
    type Result = ();
}

impl Handler<MsgOut> for Ws {
    // TODO: add proper errors
    type Result = ();

    #[cfg(not(test))]
    fn handle(&mut self, msg: MsgOut, ctx: &mut Self::Context) -> Self::Result {
        serde_json::to_string(&msg.0)
            .map(|json| ctx.text(json))
            .unwrap_or_else(|e| warn!("json msg failed: {}", e))
    }

    #[cfg(test)]
    fn handle(&mut self, _: MsgOut, _: &mut Self::Context) -> Self::Result {}
}

#[derive(Debug, Deserialize)]
enum MsgIn {
    Auth(Value),
    Subscribe(Value),
    Unsubscribe(Value),
    Controls(Value),
}

impl StreamHandler<Result<ws::Message, ws::ProtocolError>> for Ws {
    fn handle(&mut self, msg: Result<ws::Message, ws::ProtocolError>, ctx: &mut Self::Context) {
        if let Ok(ws::Message::Text(msg)) = msg {
            match serde_json::from_str(&msg) {
                Ok(MsgIn::Auth(data)) => serde_json::from_value(data)
                    .map(|data| {
                        let process = self.auth_manager.send(data).into_actor(self).map(
                            |pol_result, fself, ctx| match pol_result {
                                Ok(Ok(policy)) => {
                                    fself.auth(policy, ctx.address());
                                    let msg = json! { {"auth" : true} };
                                    ctx.notify(MsgOut(msg));
                                }
                                Ok(Err(e)) => {
                                    warn!("Authorisation error: {}", e);
                                    let msg = json! { {"auth" : false} };
                                    ctx.notify(MsgOut(msg));
                                }
                                Err(_) => error!("can't send auth msg to actor"),
                            },
                        );
                        ctx.spawn(process);
                    })
                    .unwrap_or_else(|e| {
                        warn!("bad auth request from client {}", e);
                    }),
                Ok(MsgIn::Subscribe(data)) => {
                    if let Some(manager) = self.observer_manager.as_ref() {
                        serde_json::from_value::<Subscribe>(data)
                            .map(|data| {
                                // TODO: smthing with errors here
                                let process =
                                    manager.send(data).into_actor(self).map(|res, _, _| {
                                        if let Err(e) = res {
                                            warn!("can't send msg to observer manager: {}", e);
                                        }
                                        if let Ok(Err(e)) = res {
                                            warn!("{}", e);
                                        }
                                    });

                                ctx.spawn(process);
                            })
                            .unwrap_or_else(|e| {
                                warn!("bad subscribe request from client {}", e);
                            })
                    }
                }
                Ok(MsgIn::Unsubscribe(data)) => {
                    if let Some(manager) = self.observer_manager.as_ref() {
                        serde_json::from_value::<Unsubscribe>(data)
                            .map(|data| {
                                let process =
                                    manager.send(data).into_actor(self).map(|res, _, _| {
                                        if let Err(e) = res {
                                            warn!("can't send msg to observer manager: {}", e);
                                        }
                                        if let Ok(Err(e)) = res {
                                            warn!("{}", e);
                                        }
                                    });
                                ctx.spawn(process);
                            })
                            .unwrap_or_else(|e| {
                                warn!("bad unsubscribe request from client {}", e);
                            })
                    }
                }
                Ok(MsgIn::Controls(data)) => {
                    if let Some(manager) = self.observer_manager.as_ref() {
                        serde_json::from_value::<Controls>(data)
                            .map(|data| {
                                let process =
                                    manager.send(data).into_actor(self).map(|res, _, _| {
                                        if let Err(e) = res {
                                            warn!("can't send msg to observer manager: {}", e);
                                        }
                                        if let Ok(Err(e)) = res {
                                            warn!("{}", e);
                                        }
                                    });
                                ctx.spawn(process);
                            })
                            .unwrap_or_else(|e| {
                                warn!("bad controls request from client {}", e);
                            })
                    }
                }
                Err(_) => warn!("bad request from client {}", &msg),
            }
        } else {
            warn!("bad msg from client {:?}", msg);
        };
    }
}
