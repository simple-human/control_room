use std::fs::File;
use std::io::Read;

use log::warn;
use serde_json::json;

/// Helper function for JSON format extension to include string from file.
/// Candidat for removal.
pub fn include_str(viewer: &mut serde_json::Value) -> Result<(), ()> {
    let mut file = None;
    {
        if let Some(v) = viewer.as_object_mut() {
            if v.len() == 1 {
                file = v.get("include_str").cloned();
            }
        }
    }
    if let Some(file) = file {
        file.as_str()
            .ok_or_else(|| {
                warn!("bad include_str path");
            })
            .and_then(|path| {
                let mut f =
                    File::open(path).map_err(|_| warn!("cant open include_str path: {}", &path))?;
                let mut s = String::new();
                f.read_to_string(&mut s)
                    .map_err(|_| warn!("cant read include_str path: {}", &path))?;
                *viewer = json!(&s);
                Ok(())
            })?;
    }

    if let Some(obj) = viewer.as_object_mut() {
        obj.iter_mut()
            .map(|(_, v)| include_str(v))
            .collect::<Result<(), ()>>()?;
    }

    if let Some(obj) = viewer.as_array_mut() {
        obj.iter_mut()
            .map(|v| include_str(v))
            .collect::<Result<(), ()>>()?;
    }

    Ok(())
}

/// Helper function for JSON format extension to include JSON from file.
/// Candidat for removal.
pub fn include_json(viewer: &mut serde_json::Value) -> Result<(), ()> {
    let mut file = None;
    {
        if let Some(v) = viewer.as_object_mut() {
            if v.len() == 1 {
                file = v.get("include_json").cloned();
            }
        }
    }
    if let Some(file) = file {
        file.as_str()
            .ok_or_else(|| {
                warn!("bad include_json path");
            })
            .and_then(|path| {
                let mut f = File::open(path)
                    .map_err(|_| warn!("cant open include_json path: {}", &path))?;
                let mut s = vec![];
                f.read_to_end(&mut s)
                    .map_err(|_| warn!("cant read include_json path: {}", &path))?;
                *viewer = serde_json::from_slice(&s)
                    .map_err(|e| warn!("cant parse include_json file: {}\n{}", &path, e))?;
                Ok(())
            })?;
    }

    if let Some(obj) = viewer.as_object_mut() {
        obj.iter_mut()
            .map(|(_, v)| include_json(v))
            .collect::<Result<(), ()>>()?;
    }

    if let Some(obj) = viewer.as_array_mut() {
        obj.iter_mut()
            .map(|v| include_json(v))
            .collect::<Result<(), ()>>()?;
    }

    Ok(())
}
