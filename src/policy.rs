use std::collections::HashMap;

use std::fs::File;
use std::io::Read;
use std::path::PathBuf;

use crate::error::Error;
use Error::*;

use log::warn;
use serde_derive::*;

/// Subscription permissions for a set of users and groups.
#[derive(Debug, Deserialize, Serialize, Clone)]
pub struct PolicyFormat {
    #[serde(default)]
    groups: Groups,
    users: Users,
}

type UserId = String;
type GroupId = String;
type ObservableId = String;

type Groups = HashMap<GroupId, Vec<ObservableId>>;

#[derive(Debug, Deserialize, Serialize, Clone)]
struct UserProps {
    #[serde(default)]
    groups: Vec<GroupId>,
    #[serde(default)]
    observables: Vec<ObservableId>,
}
type Users = HashMap<UserId, UserProps>;

pub type Policy = HashMap<UserId, Vec<ObservableId>>;

impl From<PolicyFormat> for Policy {
    fn from(policy_format: PolicyFormat) -> Self {
        let mut policy: Policy = HashMap::new();
        for (user, props) in &policy_format.users {
            let mut observables = vec![];
            observables.append(&mut props.observables.clone());
            for grp in &props.groups {
                policy_format
                    .groups
                    .get(grp)
                    .map(|items| observables.append(&mut items.clone()))
                    .or_else(|| {
                        warn!("Missing group in policy file: {}", &grp);
                        None
                    });
            }
            observables.sort_unstable();
            observables.dedup();
            policy.insert(user.clone(), observables);
        }
        policy
    }
}

/// Helper function to load policy rules from JSON config file.
pub fn load_policy(policy_file: PathBuf) -> Result<Policy, Error> {
    let mut policy_txt = vec![];
    let mut policy_file =
        File::open(policy_file).map_err(|_| PolicyError("can't open policy file".into()))?;
    policy_file
        .read_to_end(&mut policy_txt)
        .map_err(|_| PolicyError("can't read policy file".into()))?;

    let policy: PolicyFormat = serde_json::from_slice(&policy_txt)
        .map_err(|e| PolicyError(format!("can't parse policy file {}", e)))?;

    Ok(policy.into())
}

#[cfg(test)]
mod test {
    use super::*;
    use crate::test_mocks::policy;
    #[test]
    fn policy_protocol() {
        let policy: Policy = policy().into();

        assert_eq!(policy["user"]
            .iter()
            .find(|&x| x == "user_obs"), Some(&"user_obs".to_string()));
        assert_eq!(policy["user"]
            .iter()
            .find(|&x| x == "group_obs"), Some(&"group_obs".to_string()));
        assert_eq!(policy["user"]
            .iter()
            .find(|&x| x == "other_obs"), None);
    }
}
