use std::collections::HashMap;

use std::fs::File;
use std::io::Read;
use std::path::PathBuf;

use crate::error::Error;
use Error::*;

use crate::json_ext;

use serde_derive::*;
use serde_json::json;

/// *observables* configuration.
pub type Observables = HashMap<String, Observable>;

#[derive(Debug, Deserialize, Serialize, Clone, Eq, PartialEq)]
pub struct Observable {
    pub datasets: Vec<Dataset>,
    pub client: Client,
}

#[derive(Debug, Deserialize, Serialize, Clone, Eq, PartialEq)]
pub struct Dataset {
    pub source: String,
    #[serde(default = "default_object")]
    pub data_options: serde_json::Value,
    #[serde(default = "default_object")]
    pub view_options: serde_json::Value,
}

fn default_object() -> serde_json::Value {
    json!({})
}

#[derive(Debug, Deserialize, Serialize, Clone, Eq, PartialEq)]
pub struct Client {
    #[serde(default)]
    title: String,
    #[serde(default)]
    description: String,
    #[serde(default = "default_object")]
    options: serde_json::Value,
    #[serde(default = "default_object")]
    controls: serde_json::Value,
}

/// Helper function to load observables config from JSON file.
pub fn load_observables(obs_file: PathBuf) -> Result<Observables, Error> {
    let mut db = vec![];
    let mut db_file =
        File::open(obs_file).map_err(|_| ObservablesError("can't open observables file".into()))?;
    db_file
        .read_to_end(&mut db)
        .map_err(|_| ObservablesError("can't read observables file".into()))?;

    let mut db: serde_json::Value = serde_json::from_slice(&db)
        .map_err(|e| ObservablesError(format!("can't parse observables file {}", e)))?;

    json_ext::include_json(&mut db)
        .map_err(|_| ObservablesError("can't parse observables file".into()))?;
    json_ext::include_str(&mut db)
        .map_err(|_| ObservablesError("can't parse observables file".into()))?;

    let db: Observables = serde_json::from_value(db)
        .map_err(|e| ObservablesError(format!("can't parse observables file {}", e)))?;

    Ok(db)
}
