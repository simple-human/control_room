#![deny(clippy::all)]
#![allow(clippy::type_complexity)]
#![cfg_attr(test, allow(unused_imports, unused_variables))]
#![warn(missing_docs)]

//! A general monitoring library based on Actix framework.
//!
//! Control-room is a websocket based server that provides push messages from a set of configurable *observables* on subscription basis.
//! After user authentication the subscription process is managed by policy rules for a set of users and groups.
//! Observables data are fetched from configurable *sources*.
//!
//! # Server
//!
//! The Control-room is build as [`actix_web::Scope`] and can be added to [actix-web] based project.
//! The scope has a path '/control_room' that can be attached to any path in your application.
//!
//!```ignore
//! use control_room::ControlRoom;
//! use actix_web::{HttpServer, App};
//!
//! let cr = ControlRoom {
//!    password_db,
//!    policy,
//!    drivers,
//!    sources_config,
//!    observables,
//! };
//!
//! HttpServer::new(move || App::new().service(cr.clone().scope().unwrap()))
//!    .bind(("127.0.0.1", 8080))
//!    .unwrap()
//!    .run()
//!    .await;
//!```
//!
//! For the working example see `tests/server.rs`.
//!
//! After client connects the connection is upgraded to websocket. Then the following protocol is expected:
//!
//!```text
//!                 Client                                            Server
//!
//!{
//!  "Auth": {
//!    "user": "username",                       --->
//!    "password": "XXXXXXX"
//!  }}
//!
//!                                              <---  {auth: true/false}
//!
//!                                                    {
//!                                                      "observables": {
//!                                                        "some_observable": {
//!                                                          "title": "some title",
//!                                              <---        "description": "some description",
//!                                                          "options": { arbitrary JSON object },
//!                                                          "controls": { arbitrary JSON object }
//!                                                        },
//!                                                        ...
//!                                                      }
//!                                                    }
//!
//!{
//!  "Subscribe": {
//!    "subscriber": numeric_id,                 --->
//!    "observable": "some_observable",
//!    "controls": { arbitrary JSON object } // optional   
//!    "datasets": [numeric_id,...] // optional
//!  }
//!}
//!
//!                                                    {
//!                                                      "data": {
//!                                                        "dataset": numeric_id,
//!                                              <---      "load": "arbitrary JSON",
//!                                                        "receiver": numeric_id
//!                                                      }
//!                                                    }
//!
//!{
//!  "Controls": {
//!    "observable_id": numeric_id,
//!    "controls": { arbitrary JSON object }     --->
//!  }
//!}
//!
//!{
//!  "Unsubscribe": numeric_id                   --->
//!}
//!
//!```
//!
//!
//! [`actix_web::Scope`]: https://docs.rs/actix-web/3.3.2/actix_web/struct.Scope.html
//! [actix-web]: https://actix.rs
//!
//! # Authentication
//! 
//! For the time being a simple [`HashMap<String, String>`][std::collections::HashMap] is used for user/password storage.
//!
//! Passwords hashes are assumed to be in [Argon2] format.
//!
//! [Argon2]: https://github.com/P-H-C/phc-winner-argon2
//!
//! # Authorization
//!
//! Control-room allows subscription on per user and per group basis:
//!
//! ```rust
//! use control_room::PolicyFormat;
//! use serde_json::json;
//!
//!# fn main() {
//! let policy: PolicyFormat = serde_json::from_value(json!(  
//!  {
//!      "groups": {
//!          "users": ["group_observable1", "group_observable2"],
//!          "other": ["other_observable"]
//!      },
//!      "users": {
//!          "user": {
//!              "observables": ["user_observable"],
//!              "groups": ["users"]
//!          }
//!      }
//!  }
//! )).unwrap();
//!# }
//! ```
//!
//! # Drivers
//!
//! In terms of data sources Control-room has a modular structure. The server owns a `DriverRegister` - a collection of
//! `Drivers`. The `Driver` is a way to define a *source* of data, 
//! for example `Driver` can implement a database adapter, and actual db connection is a *source*.
//!
//! There are two traits to implement for an object to be in a register:
//! - [`Configurator`][`crate::Configurator`] - `Driver` constructor, in previous example it can be a db connection setup;
//! - [`Driver`][`crate::Driver`] - actual data fetching logic, for the db it is a query processing. 
//!   In a nutshell the result of trait method `observable` is a `Stream` of `Values` from [serde_json] crate.
//!   One of the ways to define a `Stream` is to use [async_stream].
//!
//! [async_stream]: https://docs.rs/async_stream
//! [serde_json]: https://docs.rs/serde_json
//!
//! ```rust
//! use control_room::{Driver, Configurator, DriverRegister, Error};
//! use serde_json::{json, Value};
//! use async_stream::stream;
//! use futures::stream::BoxStream;
//! use futures::StreamExt;
//! use std::sync::Arc;
//!
//! struct TestSource(String);
//! impl Driver for TestSource {
//!     fn observable(
//!         &self,
//!         options: Arc<Value>,
//!         _controls: Arc<Value>,
//!     ) -> Result<BoxStream<'static, Result<Value, Error>>, Error> {
//!         let options = serde_json::from_value::<String>((*options).clone())
//!             .map_err(|e| Error::DriverError(format!("{}", e)))?;
//!         let res = json!([self.0, options]);
//!         Ok(stream!( yield Ok(res); ).boxed())
//!     }
//! }
//! 
//! impl Configurator for TestSource {
//!     fn configure(options: Value) -> Result<Box<dyn Driver>, Error> {
//!         let text: String =
//!             serde_json::from_value(options).map_err(|e| Error::DriverError(format!("{}", e)))?;
//!         Ok(Box::new(TestSource(text)))
//!     }
//! }
//!
//! let drivers: DriverRegister = Default::default();
//! drivers.register::<TestSource>("test_source".into());
//! ```
//! 
//! # Sources
//!
//! The [`SourcesConfig`] defines a set of *sources* of data using modules registered in [`DriverRegister`].
//! The *options* object is passed to the [`Configurator`] `configure` method during driver setup.
//!
//! ```rust
//! use control_room::SourcesConfig;
//! use serde_json::json;
//! 
//! let sources_config: SourcesConfig = serde_json::from_value(json!(
//!         {
//!             "db": {
//!                      "driver": "sqlite",
//!                      "options": {
//!                          "file": "/tmp/db.sqlite"
//!                      }
//!                   },
//!             "sh": {
//!                      "driver": "shell",
//!                      "options": {
//!                          "cmd": r#"{command} | jq -R --slurp ."#
//!                      }
//!                   
//!                   }
//!          }
//!  ))
//!  .unwrap();
//!
//! ```
//!
//! # Observables
//!
//! The *observable* is a collection of *datasets* and a *client* config - client side configuration to properly render the data.
//! Each *dataset* has following fileds:
//! - *source* from `SourcesConfig`;
//! - *data_options* - optional server side config, corresponds to `options` object in [`Driver::observable`]
//!   (for example a query for db request);
//! - *view_options* - optional client side options for this particular dataset;
//!
//! *client* config has
//! - optional *title*;
//! - optional *description*;
//! - optional *options*, object to configure client side rendering;
//! - optional *controls*, a suggestion for client for default observable *controls* (query parameters for example).
//!
//! The [`Observables`] collection contains a set of observables.
//!
//! ```rust
//! use serde_json::json;
//! use control_room::Observables;
//! let observables: Observables = serde_json::from_value(json!(
//!         {
//!             "sh_test": {
//!                 "datasets": [{
//!                     "source": "sh",
//!                     "data_options":  {
//!                         "cmd": "echo test",
//!                         "shell": "/bin/sh"
//!                     }
//!                 }],
//! 
//!                 "client": {
//!                     "title": "test",
//!                     "description": "echo test",
//!                 }
//!             },
//!             "db_test": {
//!                 "datasets": [{
//!                     "source": "db",
//!                     "data_options": {
//!                         "query": "select x from data"
//!                     },
//!                     "view_options": {
//!                         "name": "x"
//!                     }
//!                 }],
//! 
//!                 "client": {
//!                     "title": "db test",
//!                 }
//!             },
//!         }
//! ))
//! .unwrap();
//! ```
//!
//! After authentication client get the JSON version of observables with server side config removed;
//!
//! # Subscription
//! 
//! The authorized client can subscribe to observables with `Subscribe` message
//!
//! ```.json
//! {
//!   "Subscribe": {
//!     "subscriber": numeric_id,
//!     "observable": "some_observable",
//!     "controls": { arbitrary JSON object } // optional   
//!     "datasets": [numeric_id,...] // optional
//!   }
//! }
//!     
//! ```
//!
//! Here 
//! - *subscriber* - a numeric unique id. This id binds data messages to client side viewer. It is used for `Controls`, `Unsubscribe` messages;
//! - *observable* - a name of observable from `Observables` config;
//! - *controls* - optional controls object for [`Driver`] `observable` method;
//! - *datasets* - optional array of datasets ids (relative to datasets array of observable) for partial subscription;
//!
//! # Controls
//!
//! The `Controls` message is used by the client to change observable parameters.
//!
//! ```json
//! {
//!   "Controls": {
//!     "observable_id": numeric_id,
//!     "controls": { arbitrary JSON object }
//!   }
//! }
//! ```
//!
//! Here 
//! - *observable_id* - subscriber id, corresponding *subscriber* from `Subscribe` message;
//! - *controls* - new controls object for [`Driver`] `observable` method.
//!
//! # Unsubscribe
//!
//! Finaly the `Unsubscribe` message is used by the client to remove subscription to observable, the only parameter here is the subscriber id.
//!
//! ```json
//! {
//!   "Unsubscribe": numeric_id
//! }
//! ```
//!
mod auth;
mod driver;
mod error;
mod json_ext;
mod observables;
mod observer_manager;
mod policy;
mod websocket;

#[cfg(test)]
mod test_mocks;

use crate::auth::AuthActor;
use crate::driver::Sources;
use crate::websocket::Ws;

pub use crate::auth::PasswordDb;
pub use crate::driver::{DriverRegister, SourcesConfig};
pub use crate::error::Error;
pub use crate::observables::Observables;
pub use crate::policy::PolicyFormat;

pub use crate::auth::load_passwd_db;
pub use crate::driver::{Configurator, Driver};
pub use crate::json_ext::{include_json, include_str};
pub use crate::observables::load_observables;
pub use crate::policy::load_policy;

use log::info;

use std::collections::HashMap;
use std::sync::Arc;

use actix::*;
use actix_web::{web, HttpRequest, HttpResponse};
use actix_web_actors::ws;

/// The main interface to Control-room.
#[derive(Clone)]
pub struct ControlRoom {
    /// a [`HashMap<String, String>`][std::collections::HashMap] storage for the user:password. The password hash is in Argon2 format
    pub password_db: PasswordDb,
    /// subscription permissions for a set of users and groups
    pub policy: PolicyFormat,
    /// a collection of [`Driver`] modules
    pub drivers: DriverRegister,
    /// data *sources* configuration
    pub sources_config: SourcesConfig,
    /// *observables* configuration
    pub observables: Observables,
}

impl ControlRoom {
    /// An [`actix_web::Scope`] for the Control-room application. It can be attached to an [actix-web] based project.
    /// The scope has a path '/control_room' that can be attached to any path in your application.
    ///
    /// [`actix_web::Scope`]: https://docs.rs/actix-web/3.3.2/actix_web/struct.Scope.html
    /// [actix-web]: https://actix.rs

    pub fn scope(self) -> Result<actix_web::Scope, Error> {
        let ControlRoom {
            password_db,
            policy,
            drivers,
            sources_config,
            observables,
        } = self;

        let auth_manager: Arc<Addr<AuthActor>> =
            Arc::new(AuthActor::new(password_db, policy.into(), observables).start());

        let mut sources = HashMap::new();

        for (k, v) in sources_config {
            drivers.spawn(v).map(|driver| {
                info!(r#"spawn source "{}"."#, &k);
                sources.insert(k, driver)
            })?;
        }

        let sources: Arc<Sources> = Arc::new(sources);

        let s = actix_web::web::scope("/control_room")
            .data((auth_manager, sources))
            .service(web::resource("").route(web::get().to(Self::index)));

        Ok(s)
    }

    async fn index(
        req: HttpRequest,
        stream: web::Payload,
    ) -> Result<HttpResponse, actix_web::Error> {
        let data = req
            .app_data::<web::Data<(Arc<Addr<AuthActor>>, Arc<Sources>)>>()
            .unwrap()
            .get_ref();
        #[cfg(not(test))]
        {
            ws::start(Ws::new(data.0.clone(), data.1.clone()), &req, stream)
        }
        #[cfg(test)]
        {
            Ok(HttpResponse::NotImplemented().finish())
        }
    }
}
