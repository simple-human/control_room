use crate::Error;
use futures::stream::BoxStream;
use serde_derive::Deserialize;
use serde_json::Value;

#[cfg(test)]
use mockall::automock;

use std::collections::HashMap;
use std::sync::{Arc, Mutex};

/// The trait is used for data *source* implementation. 
#[cfg_attr(test, automock)]
pub trait Driver {
    /// create a `Stream` of data from *source*. The [async_stream] can simplify the process.
    ///
    /// The `options`  and `controls` objects are  [`serde_json`] `Value` JSON representations wraped in [`Arc`][`std::sync::Arc`].
    ///
    /// - `options` - *observable* static server side configuration from [`Observables`][`crate::Observables`] config;
    /// - `controls` - *observable* dynamic parameters comming from the client side;
    ///
    /// [`Error::DriverError`] variant should be used for errors.
    ///
    ///
    /// [async_stream]: https://docs.rs/async_stream
    fn observable(
        &self,
        options: Arc<Value>,
        controls: Arc<Value>,
    ) -> Result<BoxStream<'static, Result<Value, Error>>, Error>;
}

#[cfg_attr(test, automock)]
/// [`Driver's`][`Driver`] constructor.
pub trait Configurator {
    /// setup new *source* of data (for example a database connection).
    ///
    /// The `options` object is a [`serde_json`] `Value` JSON representation.
    ///
    /// [`serde_json`]: https://docs.rs/serde_json
    fn configure(options: Value) -> Result<Box<dyn Driver>, Error>;
}

/// A collection of named [`Drivers`][`Driver`].
#[derive(Default, Clone)]
pub struct DriverRegister(
    Arc<Mutex<HashMap<String, Box<dyn Fn(Value) -> Result<Box<dyn Driver>, Error> + Send + Sync>>>>,
);

impl DriverRegister {
    /// register a [`Driver`] `T` with `name`
    pub fn register<T>(&self, name: String)
    where
        T: Driver + Configurator + 'static,
    {
        let mut table = self.0.lock().unwrap();
        table.insert(name, Box::new(T::configure));
    }

    pub(crate) fn spawn(&self, source: SourceConfig) -> Result<Box<dyn Driver>, Error> {
        let table = self.0.lock().unwrap();
        table
            .get(&source.driver)
            .ok_or_else(|| Error::DriverError("Driver error".into()))
            .and_then(|configure| configure(source.options.clone()))
    }
}

#[derive(Deserialize, Debug, Clone)]
pub struct SourceConfig {
    driver: String,
    options: Value,
}

pub type Sources = HashMap<String, Box<dyn Driver>>;
/// *sources* configuration.
pub type SourcesConfig = HashMap<String, SourceConfig>;

#[cfg(test)]
mod test {
    use super::*;
    use serde_json::json;

    use crate::test_mocks::MockTestDriver;


    

    #[test]
    fn test_register() {
        let driver_register = DriverRegister(
            Arc::new(Mutex::new(HashMap::new())),
        );

        driver_register.register::<MockTestDriver>("test_driver".into());
        let table = driver_register.0.lock().unwrap();
        assert!(table.contains_key("test_driver"));
    }

    #[test]
    fn test_spawn() {
        let driver_register = DriverRegister(
            Arc::new(Mutex::new(HashMap::new())),
        );

        driver_register.register::<MockTestDriver>("test_driver".into());
        let source_config = SourceConfig {
            driver: "test_driver".into(),
            options: json! {{
                "option": "test_option"
            }}
        };

        let result = driver_register.spawn(source_config);
        assert!(result.is_ok());
        let source_config = SourceConfig {
            driver: "no_driver".into(),
            options: json! {{
                "option": "test_option"
            }}
        };
        let result = driver_register.spawn(source_config);
        assert!(result.is_err());
    }
}
