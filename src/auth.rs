use actix::prelude::*;
use std::collections::HashMap;

use std::fs::File;
use std::io::Read;
use std::path::PathBuf;

use crate::error::Error;
use Error::*;

use crate::observables::{Observable, Observables};
use crate::policy::Policy;

use log::info;
use serde_derive::*;

/// user/password config. Passwords hashes are in [Argon2] format.
///
/// [Argon2]: https://github.com/P-H-C/phc-winner-argon2
pub type PasswordDb = HashMap<String, String>;

pub struct AuthActor {
    password_db: PasswordDb,
    policy_db: Policy,
    observables: Observables,
}

impl AuthActor {
    pub fn new(password_db: PasswordDb, policy_db: Policy, observables: Observables) -> Self {
        Self {
            password_db,
            policy_db,
            observables,
        }
    }
}

impl Actor for AuthActor {
    type Context = Context<Self>;
}

#[derive(Debug, Deserialize, Serialize)]
pub struct AuthRqst {
    user: String,
    password: String,
}

impl Message for AuthRqst {
    type Result = Result<Observables, String>;
}

impl Handler<AuthRqst> for AuthActor {
    type Result = Result<Observables, String>;
    fn handle(&mut self, rqst: AuthRqst, _: &mut Self::Context) -> Self::Result {
        let hash = self
            .password_db
            .get(&rqst.user)
            .ok_or(format!("unknown user \"{}\"", rqst.user))?;

        if argon2::verify_encoded(&hash, &rqst.password.as_bytes())
            .map_err(|_| format!("wrong password hash format for user \"{}\"", rqst.user))?
        {
            info!(r#"user: "{}" is authorized"#, &rqst.user);
            let mut policy: HashMap<String, Observable> = HashMap::new();
            if let Some(obs_ids) = self.policy_db.get(&rqst.user) {
                for id in obs_ids {
                    if let Some(obs) = self.observables.get(id) {
                        let id = (*id).clone();
                        let o = (*obs).clone();
                        policy.insert(id, o);
                    }
                }
            }
            Ok(policy)
        } else {
            Err(format!("wrong password for user \"{}\"", rqst.user))
        }
    }
}

/// Helper function to load [`PasswordDb`] from JSON file.
pub fn load_passwd_db(passwd_file: PathBuf) -> Result<PasswordDb, Error> {
    let mut db = vec![];
    let mut db_file =
        File::open(passwd_file).map_err(|_| PasswdDbError("can't open passwd file".into()))?;
    db_file
        .read_to_end(&mut db)
        .map_err(|_| PasswdDbError("can't read passwd file".into()))?;

    let db: PasswordDb = serde_json::from_slice(&db)
        .map_err(|e| PasswdDbError(format!("can't parse passwd file {}", e)))?;

    Ok(db)
}

#[cfg(test)]
mod test {
    use super::*;
    use serde_json::json;
    use crate::test_mocks::{policy, observables};
    use std::sync::{Arc, Mutex};

    #[test]
    fn auth_handle() -> Result<(), String> {

        let system = System::new("test");

        let r = Arc::new(Mutex::new(Err("Nothing".to_string())));
        let rc = r.clone();

        let mut password_db = HashMap::new();
        password_db.insert("user".into(), "$argon2i$v=19$m=4096,t=3,p=1$dGVlZWVlZWVzdA$hHIz+StBfCWInMPjreKDaUDSIWVNPRpHGuG9vXg0ckQ".into());

        let actor = AuthActor::new(password_db, policy().into(), observables());

        let msg_ok = AuthRqst {
            user: "user".into(),
            password: "test_password".into()
        };
        let msg_err_password = AuthRqst {
            user: "user".into(),
            password: "wrong_password".into()
        };
        let msg_err_user = AuthRqst {
            user: "other_user".into(),
            password: "test_password".into()
        };

        Arbiter::spawn(async move {
            let auth_addr = actor.start(); 
            let result = auth_addr.send(msg_ok).await;
            let response = json! ({
                "user_obs": {
                    "datasets": [{
                        "source": "source",
                        "data_options": "test options"
                    }],
                    "client": {}
                },
                "group_obs": {
                    "datasets": [{
                        "source": "source",
                        "data_options": "test options"
                    }],
                    "client": {}
                },
            });
            let response: Observables = serde_json::from_value(response).unwrap();

            let mut rc = rc.lock().unwrap();
            *rc = (async move {
                if result.unwrap().unwrap() == response {
                    Ok(())
                } else {
                    Err("wrong response on ok authorization")
                }?;

            auth_addr.send(msg_err_user).await.unwrap()
                .and(Err("wrong user authorization".into()))
                .or::<String>(Ok(()))?;

            auth_addr.send(msg_err_password).await.unwrap()
                .and(Err("wrong password authorization".into()))
                .or::<String>(Ok(()))?;
            Ok(()) 
            }).await;

        });

        System::current().stop();
        system.run().unwrap();

        let r = r.lock().unwrap();
        r.clone()
    }
}
