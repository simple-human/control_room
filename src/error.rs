use serde_derive::*;

/// Control-room error type
#[derive(Debug, Serialize, Clone)]
pub enum Error {
    /// used in case of problems with TLS
    TlsError(String),
    /// used in case of problems reading user/password db
    PasswdDbError(String),
    /// used in case of problems with Logger system
    LoggerError(String),
    /// used in case of problems with server
    ServerError(String),
    /// used in case of problems with policy config
    PolicyError(String),
    /// used in case of problems with observables config
    ObservablesError(String),
    /// used in drivers implementations
    DriverError(String),
    /// used in case of problems with sources config
    SourcesError(String),
    /// used in case of subscription problems
    SubscribeError(String),
    /// used in case of unsubscription problems
    UnsubscribeError(String),
}

impl std::error::Error for Error {}
impl actix_web::error::ResponseError for Error {}

impl std::fmt::Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let msg = match self {
            Error::TlsError(msg) => msg,
            Error::PasswdDbError(msg) => msg,
            Error::LoggerError(msg) => msg,
            Error::ServerError(msg) => msg,
            Error::PolicyError(msg) => msg,
            Error::ObservablesError(msg) => msg,
            Error::DriverError(msg) => msg,
            Error::SourcesError(msg) => msg,
            Error::SubscribeError(msg) => msg,
            Error::UnsubscribeError(msg) => msg,
        };
        write!(f, "{}", msg)
    }
}
