use actix::prelude::*;
use serde_json::Value;

use tokio::stream::StreamExt;

use std::collections::HashMap;
use std::sync::Arc;

use crate::driver::Sources;
use crate::observables::{Client, Observables};
use crate::websocket::MsgOut;

use crate::websocket::Ws;

use crate::error::Error;
use Error::*;

use log::warn;
use serde_derive::*;
use serde_json::json;

#[derive(Clone, Debug)]
struct Observer {
    handlers: HashMap<usize, SpawnHandle>,
    observable: String,
    controls: Value,
}
pub struct ObserverManager {
    observables: Observables,
    sources: Arc<Sources>,
    ws: Addr<Ws>,
    // TODO: think about i32 key ..
    streams: HashMap<i32, Observer>,
}

impl ObserverManager {
    pub fn new(observables: Observables, sources: Arc<Sources>, ws: Addr<Ws>) -> Self {
        ObserverManager {
            observables,
            sources,
            ws,
            streams: HashMap::new(),
        }
    }

    fn subscribe(&mut self, rqst: Subscribe, ctx: &mut <Self as Actor>::Context) -> Result<(), Error> {
        let Subscribe {
            subscriber,
            observable,
            controls,
            datasets,
        } = rqst;

        let observable_config = self
            .observables
            .get(&observable)
            .ok_or_else(|| SubscribeError(format!("can't find observable: {}", &observable)))?;

        let selected_datasets: Vec<_> = if let Some(datasets) = datasets {
            observable_config
                .datasets
                .iter()
                .enumerate()
                .filter(|(id, _)| datasets.contains(id))
                .collect()
        } else {
            observable_config.datasets.iter().enumerate().collect()
        };

        let mut handlers = HashMap::new();
        for (dataset_id, dataset) in selected_datasets {
            let driver = self
                .sources
                .get(&dataset.source)
                .ok_or_else(|| SubscribeError(format!("can't find source: {}", &dataset.source)))?;
            let options = &dataset.data_options;

            let stream =
                driver.observable(Arc::new(options.clone()), Arc::new(controls.clone()))?;

            let handler = Self::add_stream(
                stream.map(move |result| Response {
                    observer_id: subscriber,
                    dataset_id,
                    load: result,
                }),
                ctx,
            );
            handlers.insert(dataset_id, handler);
        }
        let observer = Observer {
            handlers,
            observable,
            controls,
        };
        self.streams.insert(subscriber, observer);

        Ok(())
    }

    fn unsubscribe(&mut self, rqst: Unsubscribe, ctx: &mut <Self as Actor>::Context) -> Result<(), Error> {
        {
            let observer = self
                .streams
                .get(&rqst.0)
                .ok_or_else(|| UnsubscribeError(format!("cant unsubscribe id ({})", &rqst.0)))?;
            for h in observer.handlers.values() {
                ctx.cancel_future(*h);
            }
        }
        self.streams
            .remove(&rqst.0)
            .ok_or_else(|| UnsubscribeError("cant unsubscribe".into()))?;
        Ok(())
    }
}

#[derive(Debug, Serialize)]
struct ClientConfig {
    client: Client,
    data_conf: Vec<Value>,
}

impl Actor for ObserverManager {
    type Context = Context<Self>;

    fn started(&mut self, ctx: &mut Self::Context) {
        // [TODO]: (optimization) collect datasets names for sending
        // collect configs for observables and send them to client
        let observables: HashMap<_, _> = self
            .observables
            .iter()
            .map(|(k, v)| {
                let data_conf = v
                    .datasets
                    .iter()
                    .map(|dataset| dataset.view_options.clone())
                    .collect();
                let client = v.client.clone();
                let config = ClientConfig { client, data_conf};
                (k, config)
            })
            .collect();
        serde_json::to_value(observables)
            .map(|observables_json| {
                let msg = json! { { "observables": observables_json } };
                let fut = self.ws.send(MsgOut(msg)).into_actor(self).map(|_, _, _| ());
                ctx.spawn(fut);
            })
            .unwrap_or(());
    }
}

#[derive(Clone)]
struct Response {
    observer_id: i32,
    dataset_id: usize,
    load: Result<serde_json::Value, Error>,
}

impl Message for Response {
    type Result = Result<(), Error>;
}

impl StreamHandler<Response> for ObserverManager {
    fn handle(&mut self, rqst: Response, ctx: &mut Self::Context) {
        let Response {
            observer_id,
            dataset_id,
            load,
        } = rqst;

        match load {
            Ok(load) => {
                let response = json! {
                    {
                        "data": {
                            "receiver": observer_id,
                            "dataset": dataset_id,
                            "load": load
                        }
                    }
                };

                let out_fut = self
                    .ws
                    .send(MsgOut(response))
                    .into_actor(self)
                    .map(|_, _, _| ());
                ctx.spawn(out_fut);
            }

            Err(e) => warn!("command failed: {}", e),
        }
    }

    fn finished(&mut self, _: &mut Self::Context) {}
}

#[derive(Deserialize, Debug)]
pub struct Subscribe {
    subscriber: i32,
    observable: String,
    #[serde(default = "default_controls")]
    controls: Value,
    #[serde(default)]
    datasets: Option<Vec<usize>>,
}

fn default_controls() -> serde_json::Value {
    json!({})
}

impl Message for Subscribe {
    type Result = Result<(), Error>;
}

impl Handler<Subscribe> for ObserverManager {
    type Result = Result<(), Error>;

    fn handle(&mut self, rqst: Subscribe, ctx: &mut Self::Context) -> Self::Result {
        self.subscribe(rqst, ctx)
    }
}

#[derive(Deserialize, Debug)]
pub struct Unsubscribe(i32);

impl Message for Unsubscribe {
    type Result = Result<(), Error>;
}

impl Handler<Unsubscribe> for ObserverManager {
    type Result = Result<(), Error>;
    fn handle(&mut self, rqst: Unsubscribe, ctx: &mut Self::Context) -> Self::Result {
        self.unsubscribe(rqst, ctx)
    }
}

#[derive(Deserialize, Debug)]
pub struct Controls {
    observable_id: i32,
    controls: Value,
}

impl Message for Controls {
    type Result = Result<(), Error>;
}

impl Handler<Controls> for ObserverManager {
    type Result = Result<(), Error>;

    fn handle(&mut self, rqst: Controls, ctx: &mut Self::Context) -> Self::Result {
        let observer = self.streams.get(&rqst.observable_id).ok_or_else(|| {
            UnsubscribeError(format!(
                "can't find observable id ({})",
                &rqst.observable_id
            ))
        })?;
        let observer = observer.clone();
        self.unsubscribe(Unsubscribe(rqst.observable_id), ctx)?;
        self.subscribe(Subscribe {
            subscriber: rqst.observable_id,
            observable: observer.observable,
            controls: rqst.controls,
            datasets: Some(observer.handlers.iter().map(|(k, _)| *k).collect()),
        }, ctx)?;
        Ok(())
    }
}

pub struct Kill;
impl Message for Kill {
    type Result = ();
}

impl Handler<Kill> for ObserverManager {
    type Result = ();

    fn handle(&mut self, _rqst: Kill, ctx: &mut Self::Context) -> Self::Result {
        ctx.stop();
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use serde_json::json;
    use crate::test_mocks::{observables, password_db, sources, policy};
    use crate::auth::AuthActor;
    // use tokio::sync::Mutex;
    use std::sync::Mutex;
    
    
    fn runner<F,R>(f: F) -> Result<(), String> 
        where F: FnOnce(Addr<ObserverManager>) -> R + 'static,
              R: Future<Output=Result<(), String>>
    {
        let system = System::new("test");

        let r = Arc::new(Mutex::new(Err("Nothing".to_string())));
        let rc = r.clone();

        Arbiter::spawn(async move {
            let sources = sources();
            let observables = observables();
            let auth_manager = AuthActor::new(password_db(), policy().into(), observables.clone()).start();
            let ws = Ws::new(Arc::new(auth_manager),sources.clone()).start();
            let observer_manager = ObserverManager::new(observables, sources, ws).start();
            let mut rc = rc.lock().unwrap();
            *rc = f(observer_manager).await;
        });

        System::current().stop();
        system.run().unwrap();

        let r = r.lock().unwrap();
        r.clone()
    }

    #[test]
    fn test_observer_manager_subscribe() -> Result<(), String> {

        runner( |observer_manager| async move {
            let subscribe: Subscribe = serde_json::from_value(json! ({
                "subscriber": 0,
                "observable": "user_obs",
                "controls": "test controls",
            })).expect("bad subscribe json");

            observer_manager.send(subscribe).await.unwrap()
                .map_err(|e| format!("subscription failed: {}", e))?;

            let subscribe: Subscribe = serde_json::from_value(json! ({
                "subscriber": 1,
                "observable": "user_obs",
                "controls": {},
            })).expect("bad subscribe json");
            if observer_manager.send(subscribe).await.unwrap().is_ok() {
                return Err("wrong controls format".into())
            }

            Ok(())
        })
    }

    #[test]
    fn test_observer_manager_unsubscribe() -> Result<(), String> {

        runner( |observer_manager| async move {
            let subscribe: Subscribe = serde_json::from_value(json! ({
                "subscriber": 0,
                "observable": "user_obs",
                "controls": "test controls",
            })).expect("bad subscribe json");

            observer_manager.send(subscribe).await.unwrap()
                .map_err(|e| format!("subscription failed: {}", e))?;
            observer_manager.send(Unsubscribe(0)).await.unwrap()
                .map_err(|e| format!("unsubscription failed: {}", e))?;
            if observer_manager.send(Unsubscribe(1)).await.unwrap().is_ok() {
                return Err("wrong unsubscribe id".to_string());
            }
            Ok(())
        })
    }

    #[test]
    fn test_observer_manager_controls() -> Result<(), String> {

        runner( |observer_manager| async move {
            let subscribe: Subscribe = serde_json::from_value(json! ({
                "subscriber": 0,
                "observable": "user_obs",
                "controls": "test controls",
            })).expect("bad subscribe json");

            observer_manager.send(subscribe).await.unwrap()
                .map_err(|e| format!("subscription failed: {}", e))?;

            let controls: Controls = serde_json::from_value(json! ({
                "observable_id": 0,
                "controls": "other controls",
            })).expect("bad controls json");

            observer_manager.send(controls).await.unwrap()
                .map_err(|e| format!("controls failed: {}", e))?;

            let controls: Controls = serde_json::from_value(json! ({
                "observable_id": 1,
                "controls": "other controls",
            })).expect("bad controls json");

            if observer_manager.send(controls).await.unwrap().is_ok() {
                return Err("wrong controls id".to_string());
            }

            let controls: Controls = serde_json::from_value(json! ({
                "observable_id": 0,
                "controls": {},
            })).expect("bad controls json");

            if observer_manager.send(controls).await.unwrap().is_ok() {
                return Err("wrong controls format".to_string());
            }

            Ok(())
        })
    }

    #[test]
    fn test_observer_manager_kill() -> Result<(), String> {
        runner( |observer_manager| async move {
            let subscribe: Subscribe = serde_json::from_value(json! ({
                "subscriber": 0,
                "observable": "user_obs",
                "controls": "test controls",
            })).expect("bad subscribe json");

            observer_manager.send(Kill).await.unwrap();

            if observer_manager.send(subscribe).await.is_ok() {
                return Err("actor is alive".to_string());
            }

            Ok(())
        })
    }
}
