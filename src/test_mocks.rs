use mockall::mock;
use crate::*;
use serde_json::{Value, json};
use std::fmt::{Formatter, Debug};
use std::sync::Arc;
use futures::stream::BoxStream;
use futures::StreamExt;
use async_stream::stream;

mock! {
    pub TestDriver {}

    impl Driver for TestDriver {
        fn observable(
            &self,
            options: Arc<Value>,
            controls: Arc<Value>,
        ) -> Result<BoxStream<'static, Result<Value, Error>>, Error>;
    }

    impl Debug for TestDriver {
        fn fmt<'a>(&self, f: &mut Formatter<'a>) -> std::fmt::Result;
    }
}

impl Configurator for MockTestDriver {
    fn configure(_options: Value) -> Result<Box<dyn Driver>, Error> {
        let mock = MockTestDriver::new();
        Ok(Box::new(mock))
    }
}

struct TestSource(String);

impl Driver for TestSource {
    fn observable(
        &self,
        options: Arc<Value>,
        controls: Arc<Value>,
    ) -> Result<BoxStream<'static, Result<Value, Error>>, Error> {
        let options = serde_json::from_value::<String>((*options).clone())
            .map_err(|e| Error::DriverError(format!("TestSource options error: {}", e)))?;
        let controls = serde_json::from_value::<String>((*controls).clone())
            .map_err(|e| Error::DriverError(format!("TestSource controls error: {}", e)))?;
        let res = json!([self.0, options, controls]);
        println!("{}",res);
        Ok(stream!( yield Ok(res); ).boxed())
    }
}

impl Configurator for TestSource {
    fn configure(options: Value) -> Result<Box<dyn Driver>, Error> {
        let text: String =
            serde_json::from_value(options).map_err(|e| Error::DriverError(format!("{}", e)))?;
        Ok(Box::new(TestSource(text)))
    }
}

pub fn policy() -> crate::policy::PolicyFormat { 
    let policy = json! (
        {
            "groups": {
                "users": ["group_obs"],
                "other": ["other_obs"]
            },
            "users": {
                "user": {
                    "observables": ["user_obs"],
                    "groups": ["users"]
                }
            }
        }
    );
    serde_json::from_value(policy).expect("wrong policy json")
}

pub fn observables() -> crate::observables::Observables {
    let observables = json! ({
        "user_obs": {
            "datasets": [{
                "source": "source",
                "data_options": "test options"
            }],
            "client": {}
        },
        "group_obs": {
            "datasets": [{
                "source": "source",
                "data_options": "test options"
            }],
            "client": {}
        },
        "other_obs": {
            "datasets": [{
                "source": "source",
                "data_options": "test options"
            }],
            "client": {}
        },
    });
    serde_json::from_value(observables).expect("wrong observables json")
}

pub fn password_db() -> crate::auth::PasswordDb {
        let mut password_db = HashMap::new();
        password_db.insert("user".into(), "$argon2i$v=19$m=4096,t=3,p=1$dGVlZWVlZWVzdA$hHIz+StBfCWInMPjreKDaUDSIWVNPRpHGuFaG9vXg0ckQ".into());
        password_db
}

pub fn sources() -> Arc<Sources> {
        let mut sources = HashMap::new();
        let driver = TestSource::configure(json! {"test options"}).expect("wrong TestSource configuration");
        sources.insert("source".into(), driver);
        Arc::new(sources)
}
