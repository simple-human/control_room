#![deny(clippy::all)]
use actix_web::*;
use async_stream::stream;
use control_room::{DriverRegister, Error};
use futures::stream::BoxStream;
use futures::StreamExt;
use rustls::internal::pemfile::{certs, rsa_private_keys};
use rustls::{NoClientAuth, ServerConfig};
use serde_json::{json, Value};
use std::fs::File;
use std::io::BufReader;
use std::sync::Arc;

use control_room::{Configurator, Driver};

// a test driver that send it's configuration to client.
struct TestSource(String);

impl Driver for TestSource {
    fn observable(
        &self,
        options: Arc<Value>,
        _controls: Arc<Value>,
    ) -> Result<BoxStream<'static, Result<Value, Error>>, Error> {
        let options = serde_json::from_value::<String>((*options).clone())
            .map_err(|e| Error::DriverError(format!("{}", e)))?;
        let res = json!([self.0, options]);
        Ok(stream!( yield Ok(res); ).boxed())
    }
}

impl Configurator for TestSource {
    fn configure(options: Value) -> Result<Box<dyn Driver>, Error> {
        let text: String =
            serde_json::from_value(options).map_err(|e| Error::DriverError(format!("{}", e)))?;
        Ok(Box::new(TestSource(text)))
    }
}

async fn server() -> Result<(), Error> {
    use control_room::ControlRoom;

    simple_logger::SimpleLogger::from_env().init().unwrap();

    let password_db = serde_json::from_value(json!(
            {"test_user": "$argon2i$v=19$m=4096,t=3,p=1$dGVlZWVlZWVzdA$hHIz+StBfCWInMPjreKDaUDSIWVNPRpHGuG9vXg0ckQ"}
        )).unwrap();

    let policy = serde_json::from_value(json!(
        {
            "groups": {
                "main": ["other"]
            },
            "users": {
                "test_user": {
                    "observables": ["test"],
                    "groups": ["main"]
                }
            }
        }
    ))
    .unwrap();

    let drivers: DriverRegister = Default::default();
    drivers.register::<TestSource>("test_source".into());

    let sources_config = serde_json::from_value(json!(
        {"echo": {
                     "driver": "test_source",
                     "options": "test source options"
                 }}
    ))
    .unwrap();

    let observables = serde_json::from_value(json!(
            {
                "test": {
                    "datasets": [{
                        "source": "echo",
                        "data_options":  "test observable options",
                        "view_options": {
                            "name": "test"
                        }
                    }],

                    "client": {
                        "title": "test",
                        "description": "echo test",
                    }
                },
                "other": {
                    "datasets": [{
                        "source": "echo",
                        "data_options": "" ,
                        "view_options": {
                            "name": "info"
                        }
                    }],

                    "client": {
                        "title": "fortune",
                        "description":  "some other observable",
                    }
                },
            }
    ))
    .unwrap();

    let certificate =
        File::open("./configs/server.crt").map_err(|e| Error::TlsError(e.to_string()))?;
    let ca_certificate =
        File::open("./configs/rootCA.crt").map_err(|e| Error::TlsError(e.to_string()))?;
    let key = File::open("./configs/server.key").map_err(|e| Error::TlsError(e.to_string()))?;

    let mut cert = BufReader::new(certificate);
    let mut ca_cert = BufReader::new(ca_certificate);
    let mut key = BufReader::new(key);

    let mut cert_chain = vec![];
    let mut server_config = ServerConfig::new(NoClientAuth::new());
    let mut cert =
        certs(&mut cert).map_err(|_| Error::TlsError("Error loading tls cert".into()))?;
    let mut ca_cert =
        certs(&mut ca_cert).map_err(|_| Error::TlsError("Error loading tls cert".into()))?;
    cert_chain.append(&mut cert);
    cert_chain.append(&mut ca_cert);
    let mut keys =
        rsa_private_keys(&mut key).map_err(|_| Error::TlsError("Error loading tls key".into()))?;
    server_config
        .set_single_cert(cert_chain, keys.remove(0))
        .map_err(|_| Error::TlsError("Wrong tls keys".into()))?;

    let cr = ControlRoom {
        password_db,
        policy,
        drivers,
        sources_config,
        observables,
    };

    HttpServer::new(move || App::new().service(cr.clone().scope().unwrap()))
        .bind_rustls("localhost:9001", server_config)
        .unwrap()
        .run()
        .await
        .unwrap();
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;
    use actix::prelude::*;
    use futures::sink::SinkExt;
    use futures::stream::StreamExt;
    use serde_json::json;
    use websocket_lite::{Message, Result};

    #[test]
    fn test_server() {
        let _ = std::thread::spawn(|| {
            System::new("test").block_on(async move {
                server().await.expect("cant start server");
            });
        });

        // wait for server to start
        std::thread::sleep(std::time::Duration::from_millis(500));

        let mut rt = tokio::runtime::Runtime::new().expect("cant start tokio runtime");

        rt.block_on(async {
            let con = websocket_lite::ClientBuilder::new("wss://localhost:9001/control_room")
                .expect("no server");
            let mut stream = con.async_connect().await.expect("no connection");

            stream
                .send(Message::text(
                    json!(
                    {"Auth":
                        {
                            "user": "test_user",
                            "password": "test_password"
                        }})
                    .to_string(),
                ))
                .await
                .expect("cant send auth");

            let msg: Option<Result<Message>> = stream.next().await;
            let msg = match msg {
                Some(Ok(msg)) => msg,
                _ => panic!("bad msg"),
            };
            assert_eq!(msg.data(), r#"{"auth":true}"#);

            // get observables
            let msg: Option<Result<Message>> = stream.next().await;

            let msg = match msg {
                Some(Ok(msg)) => msg,
                _ => panic!("bad msg"),
            };
            let msg: serde_json::Value =
                serde_json::from_slice(msg.data()).expect("can't parse observables json");
            let expected = json!(
                {
                  "observables": {
                    "other": {
                      "client": {
                        "controls": {},
                        "description": "some other observable",
                        "options": {},
                        "title": "fortune",
                      },
                      "data_conf": [
                        {
                          "name": "info"
                        }
                      ]
                    },
                    "test": {
                      "client": {
                        "controls": {},
                        "description": "echo test",
                        "options": {},
                        "title": "test",
                      },
                      "data_conf": [
                        {
                          "name": "test"
                        }
                      ]
                    }
                  }
                }
            );
            assert_eq!(msg, expected);

            stream
                .send(Message::text(
                    json!(
                    {"Subscribe":
                        {
                            "subscriber": 0,
                            "observable": "test",
                            "controls": {},
                            //"datasets": [0],
                        }})
                    .to_string(),
                ))
                .await
                .expect("cant send auth");

            let msg: Option<Result<Message>> = stream.next().await;

            let msg = match msg {
                Some(Ok(msg)) => msg,
                _ => panic!("bad msg"),
            };
            let msg: serde_json::Value =
                serde_json::from_slice(msg.data()).expect("can't parse data json");

            let expected = json!(
                {"data": {
                             "dataset": 0,
                             "load": ["test source options", "test observable options"],
                             "receiver": 0
                         }
                }
            );
            assert_eq!(msg, expected);

            stream
                .send(Message::text(
                    json!(
                    {"Unsubscribe": 0 })
                    .to_string(),
                ))
                .await
                .expect("cant send auth");
            std::thread::sleep(std::time::Duration::from_millis(100));
        });
    }
}
